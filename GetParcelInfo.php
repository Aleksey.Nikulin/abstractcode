<?php

namespace app\components\api\api_1_0\components;

use app\components\api\ApiService;
use app\models\Parcel;
use yii\base\Model;

/**
 * @Description only for api json
 *
 * Class GetParcelInfo
 * @package app\components\api\api_1_0\components
 */
class GetParcelInfo extends ApiService
{
    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    private function getParcel()
    {
        return Parcel::find()->where([
            "company_id" => $this->user['id'],
            "id" => preg_replace("/[^0-9]+/", "", $this->getVar($this->data, "track_id"))
        ])->one();
    }

    /**
     * @param $object
     * @return array
     */
    private function extractDataModel($object)
    {
        return $object ? array_map(function (Model $o) {
            return $o->attributes;
        }, $object) : [];
    }

    /**
     * @return array|mixed
     */
    private function process()
    {
        return ($parcels = $this->getParcel()) ? $parcels->attributes +
            ['products' => $this->extractDataModel($parcels->parcelProducts)] +
            ["boxes" => $this->extractDataModel($parcels->parcelBoxes)] +
            ["extend_data" => $this->extractDataModel($parcels->parcelExtendDatas)] : null;
    }

    /**
     * @return array
     * @throws \SoapFault
     */
    function run()
    {
        return ($data = $this->process()) ?
            json_decode(json_encode($data), 1) :
            $this->ApiError("Данные не найдены");
    }
}