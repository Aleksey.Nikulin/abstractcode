<?
// формирует $xml для основного файла


namespace app\modules\yaApi\components\engine;

use app\models\Company;
use app\models\Parcel;
use app\models\TrackingModel;
use app\models\TrackingStatusModel;
use app\modules\yaApi\components\YandexEngine;
use app\modules\yaApi\YandexSetting;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class getOrderHistory
 * @package app\modules\yaApi\components\engine
 */
class getOrderHistory extends YandexEngine
{
    /**
     * @var string
     */
    public $yandexId = '';

    /**
     * @var string
     */
    public $deliveryId = '';

    /**
     * @var null
     */
    public $parcel = null;

    /**
     * @var array
     */
    public $yandexOrder = [];

    /**
     * @var array
     */
    public $history = [];

    /**
     * @var null
     */
    public $company = null;

    /**
     * @return array
     */
    function process()
    {
        $this->setCompany()->setDeliveryId()->setYandexId()->checkUid()->setParcel();
        if (!$this->yandexAPI->error && $this->setHistory()) {
            return ["orderStatusHistory" => [
                "orderId" => [
                    "yandexId" => $this->yandexId,
                    "deliveryId" => ($this->getVar($this->parcel, 'id') ? $this->company['prefix'] . $this->parcel['id'] : "")
                ],
                "history" => $this->history,
            ]];
        }
    }

    /**
     * @return $this
     */
    public function setCompany()
    {
        $this->company = Company::findOne(['id' => $this->filterDeliveryId($this->yandexAPI->bb_client)]);
        return $this;
    }

    /**
     * @return $this
     */
    function setYandexId()
    {
        $this->yandexId = $this->getVar($this->getVar($this->getVar($this->input_data, 'request'), 'orderId'), 'yandexId');
        return $this;
    }

    /**
     * @return $this
     */
    function setDeliveryId()
    {
        $this->deliveryId = (int)$this->filterDeliveryId($this->getVar($this->getVar($this->getVar($this->input_data, 'request'), 'orderId'), 'deliveryId'));
        return $this;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    function getParcel()
    {
        return Parcel::find()->where(["company_id" => $this->company['id']])->andWhere($this->filter())->asArray()->one();
    }

    /**
     * @return array|string
     */
    function filter()
    {
        $criteria = new Query();
        if ($this->deliveryId) {
            $criteria->andWhere([Parcel::tableName() . '.id' => $this->deliveryId]);
        } else if ($this->yandexId) {
            $criteria->andWhere([Parcel::tableName() . '.order_id' => $this->yandexId]);
        }
        return $criteria->where ? $criteria->where : [Parcel::tableName() . ".id" => new Expression("NULL")];
    }

    /**
     * @return $this
     */
    function setParcel()
    {
        if (!($this->parcel = $this->getParcel())) {
            $this->yandexAPI->error = $this->yandexAPI->error('У вас нет прав на получение данных об этой посылке.');
        }
        return $this;
    }

    /**
     * @return array|string
     */
    function filterStatuses()
    {
        $criteria = new Query();

        if ($this->yandexId) {
            $criteria->andWhere([TrackingModel::tableName() . ".`order_id`" => $this->yandexId]);
        } else if ($this->deliveryId) {
            $criteria->andWhere([TrackingModel::tableName() . ".`boxberry_id`" => $this->company['prefix'] . $this->deliveryId]);
        }
        if ($criteria->where) {
            $criteria->andWhere([TrackingModel::tableName() . ".`im_code`" => $this->company['imcode']]);
            return $criteria->where;
        } else {
            return [TrackingModel::tableName() . ".`id`" => new Expression("NULL")];
        }
    }

    /**
     * @return array
     */
    function getStatuses()
    {
        return TrackingStatusModel::find()->where([
            TrackingStatusModel::tableName() . ".`show_site`" => "1"
        ])->andWhere($this->filterStatuses())
            ->leftJoin(TrackingModel::tableName(), [
                TrackingModel::tableName() . ".`id`" => new Expression(TrackingStatusModel::tableName() . ".`track_id`")
            ])->orderBy([TrackingStatusModel::tableName() . ".`date_time`" => SORT_ASC])->createCommand()->queryAll();
    }

    /**
     * @return bool
     */
    function setHistory()
    {
        if ($trackingStatus = $this->getStatuses()) {
            foreach ($trackingStatus as $status) {
                $this->history["orderStatus"][] = [
                    "statusCode" => $this->getVar($this->getVar(YandexSetting::$status, $status['status']), "ya_num"),
                    "setDate" => vsprintf("%sT%s", [
                        date("Y-m-d", strtotime($status['date_time'])),
                        date("H:i:s+03:00", strtotime($status['date_time']))
                    ]),
                    "message" => $this->getVar($this->getVar(YandexSetting::$status, $status['status']), "show_name")
                ];
            }
            return true;
        } else {
            $this->yandexAPI->error = $this->yandexAPI->error('Данные не найдены');
        }
        return false;
    }

    /**
     * @return $this
     */
    function checkUid()
    {
        if (!$this->yandexId && !$this->deliveryId) {
            $this->yandexAPI->error = $this->yandexAPI->error('Отсутствует идентификатор от СД (deliveryId) или ЯД (yandexId)');
        }
        return $this;
    }

    /**
     * @param $str
     * @return mixed
     */
    public function filterYandexId($str)
    {
        return preg_replace('/[^0-9a-z\\-]+/iu', '', $str);
    }

    /**
     * @param $str
     * @return int
     */
    public function filterDeliveryId($str)
    {
        return intval(preg_replace('/[^0-9]/iu', '', $str));
    }

    /**
     * @return array
     */
    function run()
    {
        return $this->process();
    }
}