<?
// формирует $xml для основного файла
// для ошибок вызывать $this->error('текст ошибки');
namespace app\modules\yaApi\components\engine;

use app\models\Company;
use app\models\Parcel;
use app\models\TrackingModel;
use app\modules\yaApi\components\YandexEngine;
use app\modules\yaApi\YandexSetting;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class getOrdersStatus
 * @package app\modules\yaApi\components\engine
 */
class getOrdersStatus extends YandexEngine{

    /**
     * @var array
     */
    public $yandexIds = [];

    /**
     * @var array
     */
    public $deliveryIds = [];

    /**
     * @var null
     */
    public $parcel = null;

    /**
     * @var array
     */
    public $statuses = [];

    /**
     * @var null
     */
    public $company = null;

    /**
     * @var array
     */
    public $ordersId = [];

    /**
     * @return $this
     */
    public function setCompany()
    {
        $this->company = Company::findOne(['id' => $this->filterDeliveryId($this->yandexAPI->bb_client)]);
        return $this;
    }

    /**
     * @return $this
     */
    function setDataFilters(){
        if($this->ordersId){
            foreach($this->ordersId as $ordersId){
                if($this->filterDeliveryId($this->getVar($ordersId,'deliveryId'))){
                    $this->deliveryIds[] = $this->filterDeliveryId($this->getVar($ordersId,'deliveryId'));
                } else {
                    $this->yandexIds[] = $this->filterYandexId($this->getVar($ordersId,'yandexId'));
                }
            }
            $this->deliveryIds = array_diff($this->deliveryIds,[""," "]);
            $this->yandexIds = array_diff($this->yandexIds,[""," "]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    function checkOrdersId(){
        if(!$this->ordersId){
            $this->yandexAPI->error = $this->yandexAPI->error("Отсутствует идентификатор от СД (deliveryId) или ЯД (yandexId)");
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function setOrdersId(){
        $this->ordersId = $this->getVar($this->getVar($this->getVar($this->input_data, 'request'), 'ordersId'),"orderId");
        if($this->getVar($this->ordersId,"yandexId")){
            $this->ordersId = [$this->ordersId];
        }
        return $this;
    }

    /**
     * @param $str
     * @return mixed
     */
    public function filterYandexId($str)
    {
        return preg_replace('/[^0-9a-z\\-]+/iu', '', $str);
    }

    /**
     * @param $str
     * @return int
     */
    public function filterDeliveryId($str)
    {
        return intval(preg_replace('/[^0-9]/iu', '', $str));
    }

    /**
     * @return array
     */
    function process(){
        $this->setCompany()->setOrdersId()->checkOrdersId()->setDataFilters();
        if(!$this->yandexAPI->error && $this->ordersStatuses()){
            return [
                "orderStatusHistories" => $this->statuses
            ];
        }
    }

    /**
     * @return array|string
     */
    function filter(){
        $criteria = new Query();
        if($this->deliveryIds){
            $criteria->andWhere([Parcel::tableName().".`id`" => $this->deliveryIds]);
        }
        if($this->yandexIds){
            $criteria->andWhere([Parcel::tableName().".`order_id`" => $this->yandexIds]);
        }
        return $criteria->where ? $criteria->where : [Parcel::tableName().".`id`" => "0"];
    }

    /**
     * @return array
     */
    function getOrdersStatus(){
        return Parcel::find()
            ->select([
                Parcel::tableName().".id",
                Parcel::tableName().".order_id",
                Parcel::tableName().".create_at",
                TrackingModel::tableName().".`last_status`",
                TrackingModel::tableName().".`last_status_time`",
            ])->leftJoin(TrackingModel::tableName(),[
                TrackingModel::tableName().".boxberry_id" => new Expression("concat(:prefix,". Parcel::tableName() .".id)",[
                    "prefix"=>$this->company['prefix']
                ])
            ])
            ->where([
                Parcel::tableName().".company_id"=>$this->company['id']
            ])->andWhere($this->filter())->createCommand()->queryAll();
    }

    /**
     * @return bool
     */
    function ordersStatuses(){
        if($ordersStatuses = $this->getOrdersStatus()){
            foreach($ordersStatuses as $statuses){
                $time = $this->getVar($statuses,'last_status_time') ? $this->getVar($statuses,'last_status_time') : $this->getVar($statuses,'create_at');
                $this->statuses['orderStatusHistory'][] = [
                    "orderId" => [
                        "yandexId" => $this->getVar($statuses,"order_id"),
                        "deliveryId" => $this->company['prefix'] . $this->getVar($statuses,"id")
                    ],
                    "history" => [
                        "orderStatus" => [
                            "statusCode" => $this->getVar($this->getVar(YandexSetting::$status,$this->getVar($statuses,'last_status')),"ya_num"),
                            "setDate" => vsprintf("%sT%s+03:00",[date("Y-m-d",strtotime($time)),date("H:i:s",strtotime($time))]),
                            "message" => $this->getVar($this->getVar(YandexSetting::$status,$this->getVar($statuses,'last_status')),"show_name")
                        ]
                    ]
                ];
            }
            return true;
        } else {
            $this->yandexAPI->error = $this->yandexAPI->error("У вас нет прав на получение данных об этой посылке (посылках).");
        }
        return false;
    }

    /**
     * @return array
     */
    function run(){
        return  $this->process();
    }
}